import { ITodo_Server } from "./types";

class ServerEmulator {
  private readonly todos: Record<string, ITodo_Server | undefined> = {};
  constructor() {
    this.todos = {};
  }

  getAllTodo = () => {
    return Object.values(this.todos);
  };

  getTodoById = (id: string) => {
    const todo: ITodo_Server | undefined = this.todos[id];
    if (todo) return todo;
    throw new Error("Not found");
  };

  createTodo = (todoDTO: Omit<ITodo_Server, "done">) => {
    if (!todoDTO.title) {
      throw new Error("не заполнено поле Title");
    }
    const todo = { ...todoDTO, done: false };
    this.todos[todoDTO.id] = todo;
    return todo;
  };

  updateTodo = (
    updateTodoDTO: Partial<ITodo_Server> & Pick<ITodo_Server, "id">,
  ) => {
    const oldTodo = this.todos[updateTodoDTO.id];
    if (!oldTodo) throw new Error("Not found");
    this.todos[updateTodoDTO.id] = { ...oldTodo, ...updateTodoDTO };
    return oldTodo;
  };

  deleteTodo = (id: string) => {
    const todo = this.todos[id];
    if (!todo) throw new Error("Not found");
    this.todos[id] = undefined;
    return todo;
  };
}

export const ServerEmulatorInstance = new ServerEmulator();
