import { TPriority } from "@globalTypes";

export interface ITodo_Server {
  id: string;
  title: string;
  description?: string;
  priority: TPriority;
  date: string;
  done: boolean;
}
