# Use Node.js as the base image
FROM node:14

# Set the working directory in the container
WORKDIR /app

# Copy package.json and package-lock.json to the container
COPY package.json ./
COPY yarn.lock ./

# Install project dependencies
RUN yarn install

# Copy the rest of the project files to the container
COPY . .

# Start the app
CMD ["yarn", "run", "dev"]