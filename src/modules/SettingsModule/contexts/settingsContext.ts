import { createContext } from "react";

export const SettingsContext = createContext({
  isJsonOn: true,
  isNewsOn: true,
  toggleNews: () => {},
  toggleJson: () => {},
});
