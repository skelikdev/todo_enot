import { useSettings } from "@modules/SettingsModule/hooks/useSettings";
import SettingsIcon from "@mui/icons-material/Settings";
import {
  Box,
  FormControlLabel,
  IconButton,
  Popover,
  styled,
} from "@mui/material";
import { Switch as SwitchMUI } from "@shared/ui/Switch/Switch";
import { MouseEventHandler, useState } from "react";

export const SettingsModal = () => {
  const [anchorEl, setAnchorEl] = useState<HTMLButtonElement | null>(null);
  const handleClick: MouseEventHandler<HTMLButtonElement> = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  const { isJsonOn, isNewsOn, toggleJson, toggleNews } = useSettings();

  return (
    <>
      <IconButton onClick={handleClick}>
        <SettingsIcon />
      </IconButton>
      <Popover
        open={!!anchorEl}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "center",
        }}
        transformOrigin={{
          vertical: "center",
          horizontal: "right",
        }}
      >
        <Box p={5}>
          <FormControlLabel
            labelPlacement="top"
            control={<Switch checked={isNewsOn} onChange={toggleNews} />}
            label="News"
          />
          <FormControlLabel
            labelPlacement="top"
            control={<Switch checked={isJsonOn} onChange={toggleJson} />}
            label="JSON"
          />
        </Box>
      </Popover>
    </>
  );
};

const Switch = styled(SwitchMUI)`
  margin: ${({ theme }) => theme.spacing(2, 0, 2, 0)};
`;
