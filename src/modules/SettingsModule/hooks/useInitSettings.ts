import { ISettingsContext } from "@modules/SettingsModule/interfaces";
import { useState } from "react";

export const useInitSettings = (): ISettingsContext => {
  const [isNewsOn, setIsMewsOn] = useState(true);
  const [isJsonOn, setIsJsonOn] = useState(true);
  const toggleNews = () => {
    setIsMewsOn(!isNewsOn);
  };
  const toggleJson = () => {
    setIsJsonOn(!isJsonOn);
  };
  return { isNewsOn, isJsonOn, toggleNews, toggleJson };
};
