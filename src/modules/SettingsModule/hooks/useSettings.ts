import { SettingsContext } from "@modules/SettingsModule/contexts/settingsContext";
import { useContext } from "react";

export const useSettings = () => {
  return useContext(SettingsContext);
};
