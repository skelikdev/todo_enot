export interface ISettingsContext {
  toggleJson: () => void;
  isNewsOn: boolean;
  isJsonOn: boolean;
  toggleNews: () => void;
}
