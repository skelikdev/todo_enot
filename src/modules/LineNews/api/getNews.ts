import { INewsResponse } from "@modules/LineNews/interfaces";

export const getNews = async (page: number): Promise<INewsResponse> => {
  return await fetch(
    `https://newsapi.org/v2/top-headlines?q=war&page=${page}&apiKey=5bd48aebd61746f18e11a98fd005de0a`,
  ).then((value) => value.json());
};
