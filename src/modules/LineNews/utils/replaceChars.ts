export const replaceChars = (strings: string[]) => {
  return strings.map((str) => str.replace(/\[\+\d+ chars\]/g, " "));
};
