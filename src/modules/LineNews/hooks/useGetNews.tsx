import { getNews } from "@modules/LineNews/api/getNews";
import { getMockNews } from "@modules/LineNews/mock";
import React from "react";
import { useQuery } from "react-query";

export const useGetNews = () => {
  const [page, setPage] = React.useState(0);

  const { data, isLoading } = useQuery("news" + page, {
    queryFn: () => {
      if (import.meta.env.DEV) {
        return getNews(page);
      }
      console.log("mockNews");
      return getMockNews(page);
    },
  });

  return { data, page, setPage, isLoading };
};
