export interface INewsResponse {
  articles?: { title?: string; content?: string | null }[];
}
