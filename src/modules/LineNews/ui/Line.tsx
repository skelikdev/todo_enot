import { useGetNews } from "@modules/LineNews/hooks/useGetNews";
import { replaceChars } from "@modules/LineNews/utils/replaceChars";
import { useSettings } from "@modules/SettingsModule/hooks/useSettings";
import { Box, Skeleton } from "@mui/material";
import { Marquee } from "@shared/ui/Marquee/Marquee";
import { Typography } from "@shared/ui/Typography/Typography";
import React, { ReactNode } from "react";

export const Line = () => {
  const { data, page, setPage, isLoading } = useGetNews();
  const { isNewsOn } = useSettings();

  const news: ReactNode[] = [];
  data?.articles?.forEach((value, index) => {
    const title = value.title ? value.title : "";
    const content = value.content ? replaceChars([value.content]) : "";
    news.push(
      <Box display="flex" key={index} alignItems="center">
        <Typography variant="h5">TITLE: </Typography>
        {title}
        <Box width="50px" />
        <Typography variant="subtitle1">CONTENT: </Typography>
        {content}
        <Box width="100px" />
      </Box>,
    );
  });
  if (!isNewsOn) {
    return <></>;
  }
  return isLoading ? (
    <Skeleton animation="wave" />
  ) : (
    <Marquee
      key={page}
      onFinish={() => {
        setPage(page + 1);
      }}
      speed={100}
    >
      {news}
    </Marquee>
  );
};
