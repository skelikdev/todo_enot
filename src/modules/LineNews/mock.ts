import { INewsResponse } from "@modules/LineNews/interfaces";

export const getMockNews = async (page: number) => {
  return newsMock;
};
export const newsMock: INewsResponse = {
  status: "ok",
  totalResults: 97,
  articles: [
    {
      source: {
        id: "news24",
        name: "News24",
      },
      author: "Compiled by Craig Taylor",
      title: "Jansen's stellar rise sees Proteas all-rounder win top ICC award",
      description:
        "After a stellar 12-month period that's seen him become a regular for the Proteas in Test cricket, all-rounder Marco Jansen was named the ICC Men's Emerging Cricketer of the Year on Wednesday.",
      url: "https://www.news24.com/sport/cricket/proteas/jansens-stellar-rise-sees-proteas-all-rounder-win-top-icc-award-20230125",
      urlToImage:
        "https://cdn.24.co.za/files/Cms/General/d/7128/c9df4e33eae645e1b37f9aa9e0605f46.jpg",
      publishedAt: "2023-01-25T17:09:36+00:00",
      content:
        "After a stellar 12-month period that's seen him become a regular for the Proteas in Test cricket, all-rounder Marco Jansen was named the ICC Men's Emerging Cricketer of the Year on Wednesday. \r\nREAD … [+2650 chars]",
    },
    {
      source: {
        id: "der-tagesspiegel",
        name: "Der Tagesspiegel",
      },
      author: "Birgit Herden",
      title: "Rätselhafte Befunde : Beschädigt Covid-19 die Immunabwehr?",
      description:
        "Manche Experten warnen, wiederholte Coronainfektionen könnten dauerhaft krankheitsanfälliger machen. Eine Spurensuche im Dschungel widersprüchlicher Befunde.",
      url: "https://www.tagesspiegel.de/ratselhafte-befunde-beschadigt-covid-19-die-immunabwehr-9235011.html",
      urlToImage: null,
      publishedAt: "2023-01-25T16:33:45+00:00",
      content:
        "Husten, Schnupfen, Kopfschmerz, Fieber und Erschöpfung  seit Wochen plagen sich die Menschen in Deutschland und anderen Ländern mit einer Welle von Atemwegserkrankungen, die zumindest vor dem Jahresw… [+196 chars]",
    },
    {
      source: {
        id: "polygon",
        name: "Polygon",
      },
      author: "Cass Marshall",
      title:
        "Dragonflight adds World of Warcraft trading post, “Storm’s Fury” event",
      description:
        "Dragonflight’s first major update, patch 10.0.5, adds World of Warcraft’s trading post feature and an event where players tackle the Primalists in a bitterly cold challenge.",
      url: "https://www.polygon.com/23569962/dragonflights-first-patch-introduces-the-world-of-warcraft-trading-post",
      urlToImage:
        "https://cdn.vox-cdn.com/thumbor/N4XqNp_JamjKWbUtrt_tKNmHFeA=/0x24:1200x652/fit-in/1200x630/cdn.vox-cdn.com/uploads/chorus_asset/file/24065456/ZRH610Q7YAUH1658953523750.jpg",
      publishedAt: "2023-01-25T16:30:49Z",
      content:
        "World of Warcraft: Dragonflight big 10.0.5 patch, out this week, introduces the Trading Post feature, a new survival-themed event called Storms Fury, and some new rewards. These mid-sized patches are… [+1259 chars]",
    },
    {
      source: {
        id: "espn",
        name: "ESPN",
      },
      author: null,
      title: "Bucks' Bobby Portis diagnosed with MCL sprain, sources say",
      description:
        "Bucks forward Bobby Portis has suffered an MCL sprain in his right knee, with the team still working through a timeline on his return, sources told ESPN.",
      url: "http://espn.go.com/nba/story/_/id/35523720/bucks-bobby-portis-diagnosed-mcl-sprain-sources-say",
      urlToImage:
        "https://a.espncdn.com/combiner/i?img=%2Fphoto%2F2021%2F0630%2Fr874474_1296x729_16%2D9.jpg",
      publishedAt: "2023-01-25T16:17:00Z",
      content:
        "Milwaukee Bucks forward Bobby Portis has suffered an MCL sprain in his right knee, sources told ESPN's Adrian Wojnarowski on Wednesday.\r\nThe Bucks are still working through a timeline on Portis' retu… [+350 chars]",
    },
    {
      title:
        "Lawyer: Virginia school administrators were warned three times in the hours before a 6-year-old shot his teacher",
      content: null,
    },
    {
      source: {
        id: "handelsblatt",
        name: "Handelsblatt",
      },
      author: "Handelsblatt",
      title: "Wirecard: Gericht lehnt Aussetzung des Verfahrens ab",
      description:
        "Die Richter sehen keine gravierenden Verstöße gegen rechtsstaatliche Prinzipien. Die Verteidiger warfen der Münchner Staatsanwaltschaft vor, der Verteidigung unter anderem wesentliche Unterlagen vorzuenthalten.",
      url: "https://www.handelsblatt.com/finanzen/steuern-recht/steuern/bilanzskandal-wirecard-gericht-lehnt-aussetzung-des-verfahrens-ab/28944234.html",
      urlToImage:
        "https://www.handelsblatt.com/images/markus-braun/28944250/2-format2003.jpg",
      publishedAt: "2023-01-25T16:05:48+00:00",
      content:
        "Markus BraunDer Ex-Chef von Wirecard betritt den Gerichtsraum in München. Gemeinsam mit ihm sind der ehemalige Chefbuchhalter und der frühere Wirecard-Geschäftsführer in Dubai wegen gewerbsmäßigen Ba… [+1418 chars]",
    },
    {
      source: {
        id: "cbs-news",
        name: "CBS News",
      },
      author: "Fox Van Allen",
      title:
        "Sam's Club New Year's 2023 deal: Last chance to get a Sam's Club membership for $25 and save big on gas",
      description:
        "There's never been a better time to join a warehouse club: Sam's Club memberships are less than half price.",
      url: "https://www.cbsnews.com/essentials/sams-club-new-years-2023-deal-get-a-sams-club-membership-for-25-and-save-big-on-gas-2023-01-25/",
      urlToImage:
        "https://assets2.cbsnewsstatic.com/hub/i/r/2022/03/31/cf66f568-4631-45ca-b7e9-1ea2ec05a502/thumbnail/1200x630/94b555c4ad4af9fed3730f6bdadb0ced/sams-club.jpg",
      publishedAt: "2023-01-25T16:05:00+00:00",
      content:
        "Getty Images\r\nWorried about inflation in 2023? You can save money on groceries, gas and more essentials with a Sam's Club membership. Now through Jan. 31, 2023 you can get a one-year membership to th… [+3593 chars]",
    },
    {
      source: {
        id: "engadget",
        name: "Engadget",
      },
      author: "https://www.engadget.com/about/editors/jon-fingas",
      title:
        "YouTube accused of using return-to-office policies to thwart union organizers | Engadget",
      description:
        "YouTube contractors in the Austin area claim the company is using return-to-office plans to bust labor organizers..",
      url: "https://www.engadget.com/youtube-contractors-union-nlrb-complaint-152056334.html",
      urlToImage:
        "https://s.yimg.com/os/creatr-images/2019-10/3604f560-f426-11e9-af36-2c4361ae4716",
      publishedAt: "2023-01-25T15:37:20.8549236Z",
      content:
        "YouTube Music contractors in the Austin area who voted to unionize are accusing their employers of abusing return-to-office policies to stifle labor organizers. The Alphabet Workers Union (AWU) has f… [+1923 chars]",
    },
    {
      source: {
        id: "four-four-two",
        name: "FourFourTwo",
      },
      author: "Greg Lea",
      title:
        "Manchester United report: Red Devils line up £44m deal for Brazilian starlet",
      description:
        "Manchester United are facing competition from Arsenal and Aston Villa for the forward",
      url: "https://www.fourfourtwo.com/news/manchester-united-report-red-devils-line-up-pound44m-deal-for-brazilian-starlet",
      urlToImage:
        "https://cdn.mos.cms.futurecdn.net/2HYQJh4RwXvzjuFWy9fsLC-1200-80.jpg",
      publishedAt: "2023-01-25T15:35:19Z",
      content:
        "Manchester United are weighing up a move for Real Betis forward Luiz Henrique, according to reports.\r\nThe Red Devils have already bolstered their attack this month with the loan addition of Wout Wegh… [+1964 chars]",
    },
    {
      source: {
        id: "news24",
        name: "News24",
      },
      author: "Jenni Evans and Lloyd Burnard",
      title: "'It is a helluva lot of money,' says judge in Jurie Roux case",
      description:
        "Former SA Rugby CEO and Stellenbosch University financial director Jurie Roux is challenging an arbitration award that he pay back R37 million he secretly moved towards university rugby programmes.",
      url: "https://www.news24.com/news24/southafrica/news/it-is-a-helluva-lot-of-money-says-judge-in-jurie-roux-case-20230125",
      urlToImage:
        "https://cdn.24.co.za/files/Cms/General/d/1759/200cca24be60487794472798be797e73.jpg",
      publishedAt: "2023-01-25T15:29:10+00:00",
      content:
        "<ul><li>Former SA Rugby CEO and Stellenbosch University financial director Jurie Roux is attempting to set aside a ruling that he pay back R37 million he shifted to rugby programmes.</li><li>The mone… [+2760 chars]",
    },
    {
      source: {
        id: "google-news-in",
        name: "Google News (India)",
      },
      author: "News Desk",
      title:
        "Padma Awards 2023: ORS Pioneer Dilip Mahalanabis Honoured With Padma Vibhushan",
      description:
        "Dilip Mahalanabis, the 87-year-old doctor from West Bengal, pioneered the wide use of ORS, which is estimated to have saved over 5 crore lives globally, according to a government release",
      url: "https://www.news18.com/news/india/padma-awards-2023-ors-pioneer-naga-social-worker-snake-catchers-among-recipients-6917335.html",
      urlToImage:
        "https://images.news18.com/ibnlive/uploads/2023/01/untitled-design-2023-01-25t210551.239-167466167116x9.jpg",
      publishedAt: "2023-01-25T15:28:00+00:00",
      content:
        "Dilip Mahalanabis, hailed as the ORS (Oral Rehydration Solution) pioneer, will be conferred with the Padma Vibhushan (posthumous) Indias second highest civilian award as the Government of India on We… [+2039 chars]",
    },
    {
      source: {
        id: "nbc-news",
        name: "NBC News",
      },
      author: "Erik Ortiz, Antonio Planas",
      title:
        "Teacher shot by 6-year-old texted dire warning to a loved one before she was wounded, source says",
      description:
        "The Virginia teacher who was shot by her 6-year-old student texted a loved one before she was wounded that the boy was armed and that school officials were failing to act, according to a source close to the situation.",
      url: "https://www.nbcnews.com/news/us-news/teacher-shot-6-year-old-texted-dire-warning-loved-one-was-wounded-sour-rcna67290",
      urlToImage:
        "https://media-cldnry.s-nbcnews.com/image/upload/t_nbcnews-fp-1200-630,f_auto,q_auto:best/rockcms/2023-01/230110-abigail-zwerner-al-0811-3a81c7.jpg",
      publishedAt: "2023-01-25T15:24:20Z",
      content:
        "The Virginia teacher who was shot by her 6-year-old student texted a loved one before she was wounded that the boy was armed and that school officials were failing to act, according to a source close… [+5343 chars]",
    },
    {
      source: {
        id: "bbc-news",
        name: "BBC News",
      },
      author: "BBC News",
      title:
        "Oscars 2023: Till director Chinonye Chukwu calls out misogyny and racism after snub",
      description:
        'Chinonye Chukwu, who wrote and directed Till, says there\'s "unabashed misogyny towards Black women".',
      url: "http://www.bbc.co.uk/news/entertainment-arts-64396730",
      urlToImage:
        "https://ichef.bbci.co.uk/news/1024/branded_news/146F5/production/_128410738_gettyimages-1243834162.jpg",
      publishedAt: "2023-01-25T15:22:28.4644541Z",
      content:
        'Director Chinonye Chukwu has accused Hollywood of "unabashed misogyny towards Black women" after her film Till missed out on an Oscar nomination. \r\nTill is based on the true story of the mother who p… [+3268 chars]',
    },
    {
      source: {
        id: "google-news-uk",
        name: "Google News (UK)",
      },
      author: "https://www.facebook.com/bbcnews",
      title:
        "Ukraine war: Biden to speak after German decision to send tanks to Ukraine - BBC News",
      description:
        "The US president is expected to join Germany in sending its own advanced combat vehicles to Ukraine.",
      url: "https://www.bbc.co.uk/news/live/world-europe-64396659",
      urlToImage:
        "https://m.files.bbci.co.uk/modules/bbc-morph-news-waf-page-meta/5.3.0/bbc_news_logo.png",
      publishedAt: "2023-01-25T15:21:55+00:00",
      content:
        "At a bus stop in the centre of the city of Zaporizhzhia in south-eastern Ukraine this morning, most commuters were enthusiastic about the news foreign tanks might soon be on their way to the Ukrainia… [+820 chars]",
    },
    {
      source: {
        id: "usa-today",
        name: "USA Today",
      },
      author: null,
      title:
        "Tornado watch: 'Extremely dangerous' tornado slams Texas; warnings sweep across Florida",
      description:
        "In Texas, at least one tornado tore off roofs east of Houston, downing utility poles and flipping cars and even a train. What you can expect today.",
      url: "https://www.usatoday.com/story/news/nation/2023/01/25/live-updates-tornado-watch-warning-storm-south-texas-florida/11118610002/",
      urlToImage:
        "https://www.gannett-cdn.com/presto/2023/01/25/USAT/630ad2d6-cda6-4db3-a243-6a9edff0034e-AP23024852120569.jpg?auto=webp&crop=3769,2120,x1,y114&format=pjpg&width=1200",
      publishedAt: "2023-01-25T15:17:02+00:00",
      content:
        "More than 200,000 homes and businesses were in the dark across Texas, Arkansas and Missouri on Wednesday and a tornado watch was in effect across much of Florida as a line of severe weather brought h… [+3537 chars]",
    },
    {
      source: {
        id: "google-news-in",
        name: "Google News (India)",
      },
      author: null,
      title:
        '"Disrespectful": S Jaishankar On Mike Pompeo Remarks On Sushma Swaraj',
      description:
        'Former US Secretary of State Mike Pompeo has said he never saw his counterpart Sushma Swaraj as an "important political player" but got along famously with External Affairs Minister S Jaishankar, hitting it off with him in their first meeting itself.',
      url: "https://www.ndtv.com/india-news/ex-us-top-official-mike-pompeo-says-he-never-saw-sushma-swaraj-as-an-important-political-player-3721266",
      urlToImage:
        "https://c.ndtvimg.com/vvfn30fc_mike-pompeo-sushma-swaraj-twitter_625x300_06_September_18.jpg",
      publishedAt: "2023-01-25T14:53:00+00:00",
      content:
        "Sushma Swaraj served as external affairs minister in the first Modi government from May 2014 to May 2019.\r\nWashington: Former US Secretary of State Mike Pompeo has said he never saw his counterpart S… [+3238 chars]",
    },
    {
      source: {
        id: "bbc-sport",
        name: "BBC Sport",
      },
      author: null,
      title: "Tottenham sign Danjuma on loan from Villarreal",
      description:
        "Tottenham Hotspur sign Netherlands forward Arnaut Danjuma on loan from Villarreal until the end of the season.",
      url: "http://www.bbc.co.uk/sport/football/64358264",
      urlToImage:
        "https://ichef.bbci.co.uk/live-experience/cps/624/cpsprodpb/11787/production/_124395517_bbcbreakingnewsgraphic.jpg",
      publishedAt: "2023-01-25T14:52:40.293056Z",
      content:
        "Tottenham Hotspur have signed Netherlands forward Arnaut Danjuma on loan from Villarreal until the end of the season.\r\nThe 25-year-old becomes Spurs' first signing of the January transfer window.\r\nHe… [+207 chars]",
    },
    {
      source: {
        id: "news24",
        name: "News24",
      },
      author: "Herman Mostert",
      title:
        "Dayimani extends Stormers stay: 'A special player who brings something unique'",
      description:
        "Dynamic loose forward Hacjivah Dayimani has signed a new deal with the Stormers that will see him continue to ply his trade in Cape Town until at least 2025.",
      url: "https://www.news24.com/sport/rugby/unitedrugbychampionship/dayimani-extends-stormers-stay-a-special-player-who-brings-something-unique-20230125",
      urlToImage:
        "https://cdn.24.co.za/files/Cms/General/d/648/bb0f929e99124326b5002264088837ef.jpg",
      publishedAt: "2023-01-25T14:49:28+00:00",
      content:
        "Dynamic loose forward Hacjivah Dayimani has signed a new deal with the Stormers that will see him continue to ply his trade in Cape Town until at least 2025.\r\nThe 25-year-old has been a star for the … [+990 chars]",
    },
    {
      source: {
        id: "google-news-uk",
        name: "Google News (UK)",
      },
      author: "Jamie Grierson",
      title:
        "Plymouth gunman’s father tells inquest he warned police about shotgun licence",
      description:
        "Father says he tried to stop Jake Davison getting firearm and alerted authorities about son’s ‘volatile environment’",
      url: "https://www.theguardian.com/uk-news/2023/jan/25/plymouth-gunman-jake-davison-father-inquest-warned-police-shotgun-licence",
      urlToImage:
        "https://i.guim.co.uk/img/media/70a1e7044b1b559f4feff80c34dc2d12a36eb2e3/0_713_2978_1787/master/2978.jpg?width=1200&height=630&quality=85&auto=format&fit=crop&overlay-align=bottom%2Cleft&overlay-width=100p&overlay-base64=L2ltZy9zdGF0aWMvb3ZlcmxheXMvdGctZGVmYXVsdC5wbmc&enable=upscale&s=34ab455a5983e1947296f4e58222c74d",
      publishedAt: "2023-01-25T14:45:00+00:00",
      content:
        "The father of the Plymouth gunman Jake Davison said he tried to warn police not to give his son a shotgun licence because he lived in a volatile environment.\r\nDavison used a pump-action Weatherby sho… [+1966 chars]",
    },
    {
      source: {
        id: "espn",
        name: "ESPN",
      },
      author: null,
      title:
        "Jalen Hurts, Justin Jefferson, Patrick Mahomes among MVP finalists",
      description:
        "Josh Allen, Joe Burrow, Jalen Hurts, Justin Jefferson and Patrick Mahomes are the finalists for the 2022 Associated Press NFL MVP Award.",
      url: "http://espn.go.com/nfl/story/_/id/35523019/jalen-hurts-justin-jefferson-patrick-mahomes-mvp-finalists",
      urlToImage:
        "https://a3.espncdn.com/combiner/i?img=%2Fphoto%2F2022%2F1017%2Fr1076860_1296x729_16%2D9.jpg",
      publishedAt: "2023-01-25T14:44:00Z",
      content:
        "Jalen Hurts, Justin Jefferson and Patrick Mahomes are finalists for The Associated Press 2022 NFL MVP and Offensive Player of the Year awards.\r\nBuffalo Bills quarterback Josh Allen and Cincinnati Ben… [+3925 chars]",
    },
  ],
} as INewsResponse;
