import dayjs from "dayjs";
import { DATE_FORMAT } from "../constants";
import { ITodo_Frontend } from "../interfaces";

export const useSemanticDateName = (
  todosByDate: Record<string, ITodo_Frontend[]>,
): {
  todayTodos: ITodo_Frontend[] | undefined;
  tomorrowTodos: ITodo_Frontend[] | undefined;
  futureTodos: Record<string, ITodo_Frontend[]>;
} => {
  const todayDate = dayjs(new Date()).format(DATE_FORMAT);
  const tomorrowDate = dayjs(new Date()).add(1, "day").format(DATE_FORMAT);
  const todayTodos: ITodo_Frontend[] | undefined = todosByDate?.[todayDate];
  const tomorrowTodos: ITodo_Frontend[] | undefined = todosByDate[tomorrowDate];
  const todoCopy = { ...todosByDate };

  delete todoCopy[todayDate];
  delete todoCopy[tomorrowDate];

  return { todayTodos, tomorrowTodos, futureTodos: todoCopy };
};
