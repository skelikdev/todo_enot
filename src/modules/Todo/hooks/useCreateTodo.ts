import { useSnackbar } from "notistack";
import { useMutation, useQueryClient } from "react-query";
import { createTodo } from "../api/todo";
import { EQueryKey } from "../interfaces";

export const useCreateTodo = () => {
  const { enqueueSnackbar } = useSnackbar();
  const queryClient = useQueryClient();
  const { data: createdTodo, mutate: createTodoMutation } = useMutation(
    createTodo,
    {
      onSuccess: (_data, variables) => {
        queryClient.invalidateQueries(EQueryKey.AllTodo);
        enqueueSnackbar(`Todo "${variables.title}" successfully created`, {
          variant: "success",
        });
      },
      onError: (error: Error) => {
        enqueueSnackbar(`${error?.message}`, { variant: "error" });
      },
    },
  );

  return { createTodoMutation, createdTodo };
};
