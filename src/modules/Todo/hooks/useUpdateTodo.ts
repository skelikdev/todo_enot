import { updateTodo } from "@modules/Todo/api/todo";
import { useSnackbar } from "notistack";
import { useMutation, useQueryClient } from "react-query";
import { EQueryKey } from "../interfaces";

export const useUpdateTodo = () => {
  const { enqueueSnackbar } = useSnackbar();
  const queryClient = useQueryClient();
  const { data: updatedTodo, mutate: updateTodoMutation } = useMutation(
    updateTodo,
    {
      onSuccess: (_data) => {
        queryClient.invalidateQueries(EQueryKey.AllTodo);
        enqueueSnackbar(`Todo successfully updated`, {
          variant: "success",
        });
      },
      onError: (error: Error) => {
        enqueueSnackbar(`${error?.message}`, { variant: "error" });
      },
    },
  );

  return { updateTodoMutation, updatedTodo };
};
