import { EPriority } from "@components/PriorityRadio/enums";
import dayjs, { Dayjs } from "dayjs";
import React, { useState } from "react";
import { DATE_FORMAT } from "../constants";

export const useCreateTodoState = () => {
  const [withDescription, setWithDescription] = useState(false);
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [checkedPriority, setCheckedPriority] = useState<EPriority>(
    EPriority.UNKNOWN,
  );
  const [date, setDate] = React.useState<Dayjs>(dayjs(new Date()));

  const handlerTitleChange: React.ChangeEventHandler<HTMLInputElement> = (
    event,
  ) => {
    setTitle(event.currentTarget.value);
  };

  const handlerWithDescriptionChange = () => {
    setWithDescription(!withDescription);
  };

  const handlerDescriptionChange: React.ChangeEventHandler<HTMLInputElement> = (
    event,
  ) => {
    setDescription(event.currentTarget.value);
  };

  const handlerDateChange = (newValue: Dayjs | null) => {
    if (!newValue) return;
    setDate(newValue);
  };

  return {
    title: { title, handlerTitleChange },
    withDescription: { withDescription, handlerWithDescriptionChange },
    description: { description, handlerDescriptionChange },
    priority: { checkedPriority, setCheckedPriority },
    date: { date, handlerDateChange },
    summary: {
      title,
      description,
      priority: checkedPriority,
      date: dayjs(date).format(DATE_FORMAT),
    },
  };
};
