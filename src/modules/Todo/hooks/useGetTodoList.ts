import { ITodo_Frontend } from "../interfaces";
import { useGetAllTodo } from "./useGetAllTodo";

export const useGetTodoList = () => {
  const { allTodo, isLoadingAllTodo } = useGetAllTodo();

  const todosByDate: Record<string, ITodo_Frontend[]> = {};

  allTodo?.forEach((todo) => {
    if (!todo) return;
    if (todosByDate[todo.date]) {
      todosByDate[todo.date].push(todo);
    } else {
      todosByDate[todo.date] = [todo];
    }
  });

  return { todosByDate, isLoadingAllTodo };
};
