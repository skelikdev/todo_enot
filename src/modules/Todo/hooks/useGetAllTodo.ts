import { getAllTodo } from "@modules/Todo/api/todo";
import { useSnackbar } from "notistack";
import { useQuery } from "react-query";
import { EQueryKey } from "../interfaces";

export const useGetAllTodo = () => {
  const { enqueueSnackbar } = useSnackbar();
  const { data, isLoading } = useQuery(EQueryKey.AllTodo, {
    queryFn: getAllTodo,
    onError: (error: Error) => {
      enqueueSnackbar(`${error?.message}`, { variant: "error" });
    },
  });

  return { allTodo: data, isLoadingAllTodo: isLoading };
};
