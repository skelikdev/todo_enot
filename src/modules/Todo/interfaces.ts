import { ITodo_Server } from "serverEmulator/types";

export type ITodo_Frontend = ITodo_Server;
export type TCreateTodoDto = Omit<ITodo_Server, "done">;
export type TUpdateTodoDto = Partial<ITodo_Server> & Pick<ITodo_Server, "id">;

export enum EQueryKey {
  AllTodo = "allTodo",
}
