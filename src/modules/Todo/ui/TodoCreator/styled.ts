import { styled } from "@mui/material";

export const InputsBox = styled("div")`
  margin-bottom: ${({ theme }) => theme.spacing(2)};
  display: flex;
  flex-direction: column;
  width: 100%;
  align-items: center;
`;

export const InputBox = styled("div")`
  margin-bottom: ${({ theme }) => theme.spacing(2)};
  display: flex;
  width: 100%;
  align-items: center;
`;
