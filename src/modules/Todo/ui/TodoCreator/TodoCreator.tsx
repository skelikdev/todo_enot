import { PriorityRadio } from "@components/PriorityRadio/PriorityRadio";
import { Button, Checkbox, Tooltip } from "@mui/material";
import TextField from "@mui/material/TextField";
import { LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { MobileDatePicker } from "@mui/x-date-pickers/MobileDatePicker";
import { PaperBox } from "@shared/ui/PaperBox/PaperBox";
import React from "react";
import { DATE_FORMAT } from "../../constants";
import { useCreateTodo } from "../../hooks/useCreateTodo";
import { useCreateTodoState } from "../../hooks/useCreateTodoState";
import { InputBox, InputsBox } from "./styled";

export const TodoCreator = () => {
  const {
    title: { title, handlerTitleChange },
    withDescription: { withDescription, handlerWithDescriptionChange },
    description: { description, handlerDescriptionChange },
    priority: { checkedPriority, setCheckedPriority },
    date: { date, handlerDateChange },
    summary,
  } = useCreateTodoState();

  const { createTodoMutation } = useCreateTodo();

  const handlerCreateTodo = () => {
    // честно говоря мне лень создавать валидацию итд. считаю что надо подключать formik или final-form но так как в стеке не было указано я не стал подключать
    createTodoMutation({ ...summary, id: new Date().toString() });
  };

  return (
    <PaperBox>
      <InputsBox>
        <InputBox>
          <TextField
            value={title}
            fullWidth
            onChange={handlerTitleChange}
            label="Title"
            size="small"
          />
          <Tooltip title="description">
            <Checkbox
              checked={withDescription}
              onChange={handlerWithDescriptionChange}
            />
          </Tooltip>
        </InputBox>
        <InputBox>
          {withDescription && (
            <TextField
              value={description}
              onChange={handlerDescriptionChange}
              label="Description"
              size="small"
              multiline
            />
          )}
        </InputBox>
        <PriorityRadio
          checkedPriority={checkedPriority}
          setCheckedPriority={setCheckedPriority}
        />
        <LocalizationProvider dateAdapter={AdapterDayjs}>
          <MobileDatePicker
            label="Date mobile"
            inputFormat={DATE_FORMAT}
            value={date}
            onChange={handlerDateChange}
            renderInput={(params) => (
              <TextField {...params} size="small" fullWidth />
            )}
          />
        </LocalizationProvider>
      </InputsBox>
      <Button variant="contained" onClick={handlerCreateTodo} fullWidth>
        Create
      </Button>
    </PaperBox>
  );
};
