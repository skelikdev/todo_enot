import { Accordion } from "@components/Accordion/Accordion";
import { TodoCard } from "@components/TodoCard/TodoCard";
import { useCallback } from "react";
import { useGetTodoList } from "../../hooks/useGetTodoList";
import { useSemanticDateName } from "../../hooks/useSemanticDateName";
import { useUpdateTodo } from "../../hooks/useUpdateTodo";
import * as Styled from "./styled";

export const TodoList = () => {
  const { todosByDate, isLoadingAllTodo } = useGetTodoList();
  const { todayTodos, tomorrowTodos, futureTodos } =
    useSemanticDateName(todosByDate);
  const futureTodoKeys = Object.keys(futureTodos);

  const { updateTodoMutation } = useUpdateTodo();

  const handlerChangeIsOpen = async (id: string, done: boolean) => {
    await updateTodoMutation({ id, done });
  };

  return (
    <Styled.Wrapper>
      {todayTodos && (
        <Accordion title="Today Tasks">
          {todayTodos.map(({ title, description, priority, id, done }) => (
            <TodoCard
              handlerChangeIsOpen={handlerChangeIsOpen}
              isDone={done}
              priorityStatus={priority}
              key={id}
              id={id}
              title={title}
              description={description ? description : title}
            />
          ))}
        </Accordion>
      )}
      {tomorrowTodos && (
        <Accordion title="Tomorrow Tasks">
          {tomorrowTodos.map(({ title, description, priority, id, done }) => (
            <TodoCard
              isDone={done}
              handlerChangeIsOpen={handlerChangeIsOpen}
              priorityStatus={priority}
              key={id}
              id={id}
              title={title}
              description={description ? description : title}
            />
          ))}
        </Accordion>
      )}
      {futureTodoKeys.map((date) => (
        <Accordion title={date} key={date}>
          {futureTodos[date].map(
            ({ priority, id, title, description, done }) => (
              <TodoCard
                isDone={done}
                handlerChangeIsOpen={handlerChangeIsOpen}
                priorityStatus={priority}
                key={id}
                id={id}
                title={title}
                description={description ? description : title}
              />
            ),
          )}
        </Accordion>
      ))}
    </Styled.Wrapper>
  );
};
