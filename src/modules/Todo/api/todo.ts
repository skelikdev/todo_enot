import { ServerEmulatorInstance } from "serverEmulator/serverEmulator";
import { TCreateTodoDto, TUpdateTodoDto } from "../interfaces";

export const getAllTodo = async () => {
  return ServerEmulatorInstance.getAllTodo();
};

export const getTodoById = async (id: string) => {
  return ServerEmulatorInstance.getTodoById(id);
};

export const createTodo = async (todoDTO: TCreateTodoDto) => {
  return ServerEmulatorInstance.createTodo(todoDTO);
};

export const updateTodo = async (updateTodoDTO: TUpdateTodoDto) => {
  return ServerEmulatorInstance.updateTodo(updateTodoDTO);
};

export const deleteTodo = async (id: string) => {
  return ServerEmulatorInstance.deleteTodo(id);
};
