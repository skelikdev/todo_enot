export type TSpacingFunc = (
  ...args: [number] | [number, number] | [number, number, number, number]
) => string;
