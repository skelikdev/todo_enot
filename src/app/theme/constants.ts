import { createTheme, ThemeOptions } from "@mui/material";
import { spacingFunc } from "./spacingFunc";

const h4 = {
  fontFamily: "Actor",
  fontSize: "36px",
  fontWeight: 400,
  lineHeight: "43px",
  letterSpacing: "0em",
  textAlign: "left",
};
const h5 = {
  fontFamily: "Abhaya Libre SemiBold",
  fontSize: "24px",
  fontWeight: 600,
  lineHeight: "28px",
  letterSpacing: "0em",
  textAlign: "left",
};

const body1 = {
  fontFamily: "Abhaya Libre SemiBold",
  fontSize: "14px",
  fontWeight: 600,
  lineHeight: "16.52px",
  letterSpacing: "0em",
  textAlign: "left",
};

export const customTypography = {
  h4,
  h5,
  body1,
};

const darkContrastText = "#0E0E0E";
const whiteContrastText = "#FFFFFF";

const green = {
  main: "#10C200",
};
const white = {
  main: "#F4F4F4",
};
const gray = {
  light: "#A9A9A9",
};

const blue = {
  main: "#366EFF",
};

export const colors = {
  white,
  gray,
  green,
  blue,
};

export const priorityColors = {
  critical: {
    main: "#FF0000",
    light: "#FF3333",
    dark: "#B20000",
    contrastText: whiteContrastText,
  },
  high: {
    main: "#f77107",
    light: "#F88D38",
    dark: "#AC4F04",
    contrastText: darkContrastText,
  },
  middle: {
    main: blue.main,
    light: "#5E8BFF",
    dark: "#254DB2",
    contrastText: whiteContrastText,
  },
  low: {
    main: "#FFEB33",
    light: "#FFEF5B",
    dark: "#B2A423",
    contrastText: darkContrastText,
  },
  unknown: {
    main: "#A9A9A9",
    light: "#d3d3d3",
    dark: "#6e6e6e",
    contrastText: "#2f2f2f",
  },
};
export const background = {
  default: "#222222",
  paper: "#282828",
  superDark: "#050505",
};

export const themeOptions: ThemeOptions = {
  palette: { mode: "dark" },
  spacing: spacingFunc,
  customTypography,
  customPalette: {
    background,
    colors,
    priority: priorityColors,
  },
};

export const theme = createTheme(themeOptions);
