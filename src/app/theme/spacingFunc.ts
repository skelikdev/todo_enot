import { TSpacingFunc } from "./types";

export const spacingFunc: TSpacingFunc = (...args) => {
  const spacingString = args.map((value) => `${value * 4}px`);
  return spacingString.join(" ");
};
