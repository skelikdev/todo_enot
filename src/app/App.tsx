import {
  SettingsContext,
  useInitSettings,
} from "@modules/SettingsModule/publicApi";
import { CssBaseline, styled, ThemeProvider } from "@mui/material";
import { JsonViewer } from "@pages/JsonViewer/JsonViewer";
import { MainPage } from "@pages/MainPage/MainPage";
import { SnackbarProvider } from "notistack";
import { QueryClient, QueryClientProvider } from "react-query";
import { theme } from "./theme/constants";

const queryClient = new QueryClient({});
export const App = () => {
  const initSettings = useInitSettings();
  return (
    <ThemeProvider theme={theme}>
      <QueryClientProvider client={queryClient}>
        <SettingsContext.Provider value={initSettings}>
          <SnackbarProvider maxSnack={3}>
            <AppWrapper>
              <CssBaseline />
              <MainPage />
              <JsonViewer />
            </AppWrapper>
          </SnackbarProvider>
        </SettingsContext.Provider>
      </QueryClientProvider>
    </ThemeProvider>
  );
};

export const AppWrapper = styled("div")`
  width: 100vw;
  height: 100vh;
  display: flex;
  align-items: center;
  justify-content: center;
  background: ${({ theme }) => theme.customPalette.background.superDark};
`;
