import {
  Theme as DefaultTheme,
  ThemeOptions as DefaultThemeOptions,
} from "@mui/material/styles/createTheme";
import {
  background,
  colors,
  customTypography,
  priorityColors,
} from "./theme/constants";

declare module "@mui/material/styles/createTheme" {
  interface Theme extends DefaultTheme {
    customTypography: typeof customTypography;
    customPalette: {
      colors: typeof colors;
      priority: typeof priorityColors;
      background: typeof background;
    };
  }
  interface ThemeOptions extends DefaultThemeOptions {
    customTypography: typeof customTypography;
    customPalette: {
      colors: typeof colors;
      priority: typeof priorityColors;
      background: typeof background;
    };
  }
}

type TPriority = "critical" | "high" | "middle" | "low" | "unknown";
