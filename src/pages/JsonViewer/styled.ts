import { styled } from "@mui/material";
import { ScrollBox } from "@shared/ui/ScrollBox/ScrollBox";

export const Wrapper = styled(ScrollBox)`
  width: 100%;
  max-width: 500px;
  max-height: 500px;
  overflow-y: scroll;
  padding: ${({ theme }) => theme.spacing(5)};
  margin: ${({ theme }) => theme.spacing(5)};
  border-radius: 10px;
  position: fixed;
  background: ${({ theme }) => theme.customPalette.colors.white.main};
  right: 0;
`;
