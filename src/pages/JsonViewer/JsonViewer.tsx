import { useSettings } from "@modules/SettingsModule/publicApi";
import { useGetAllTodo } from "@modules/Todo/publicApi";
import ReactJson from "react-json-view";
import { Wrapper } from "./styled";

export const JsonViewer = () => {
  const { allTodo } = useGetAllTodo();
  const { isJsonOn } = useSettings();
  if (allTodo && isJsonOn) {
    return (
      <Wrapper>
        <ReactJson
          src={allTodo}
          onEdit={(edit) => {
            console.log(edit);
          }}
          collapsed={true}
        />
        ;
      </Wrapper>
    );
  }
  return <></>;
};
