import { Line } from "@modules/LineNews/publicApi";
import { SettingsModal } from "@modules/SettingsModule/publicApi";
import { TodoCreator, TodoList } from "@modules/Todo/publicApi";
import { Box, Typography } from "@mui/material";
import { ScrollBox } from "@shared/ui/ScrollBox/ScrollBox";
import * as Styled from "./styled";

export const MainPage = () => {
  return (
    <Styled.MainPageMobileMock>
      <Styled.PaddingBox display="flex" justifyContent="space-between">
        <Box pl={4}>
          <Typography variant="h4">To Do</Typography>
        </Box>
        <SettingsModal />
      </Styled.PaddingBox>
      <Styled.PaddingBox>
        <TodoCreator />
      </Styled.PaddingBox>
      <ScrollBox>
        <TodoList />
      </ScrollBox>
      <Line />
    </Styled.MainPageMobileMock>
  );
};
