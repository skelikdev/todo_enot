import { Box, styled } from "@mui/material";

export const PaddingBox = styled(Box)`
  padding: ${({ theme }) => theme.spacing(5, 5, 0, 5)};
`;
export const MainPageMobileMock = styled("div")`
  margin: ${({ theme }) => theme.spacing(15)};
  display: flex;
  flex-direction: column;
  width: 390px;
  height: 844px;
  border-radius: 30px;
  overflow-y: hidden;
  background: ${({ theme }) => theme.customPalette.background.default};
`;
