export enum EPriority {
  CRITICAL = "critical",
  HIGH = "high",
  MIDDLE = "middle",
  LOW = "low",
  UNKNOWN = "unknown",
}
