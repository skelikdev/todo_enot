import {
  FormControl,
  FormControlLabel,
  FormLabel,
  RadioGroup,
} from "@mui/material";
import { FC } from "react";
import { EPriority } from "./enums";
import { IPriorityRadioProps } from "./interfaces";
import { Radio } from "./styled";

export const PriorityRadio: FC<IPriorityRadioProps> = ({
  checkedPriority,
  setCheckedPriority,
}) => {
  const priorities = Object.values(EPriority);
  return (
    <FormControl>
      <FormLabel id="priority-change-label">Priority</FormLabel>
      <RadioGroup row aria-labelledby="priority-change-label">
        {priorities.map((priority) => (
          <FormControlLabel
            key={priority}
            value="female"
            control={
              <Radio
                aria-label={`${priority}-priority-new-todo`}
                size="small"
                priority={priority}
                checked={checkedPriority === priority}
                onChange={() => {
                  setCheckedPriority(priority);
                }}
              />
            }
            label={priority}
          />
        ))}
      </RadioGroup>
    </FormControl>
  );
};
