import { EPriority } from "./enums";

export interface IRadioProps {
  priority: EPriority;
}

export interface IPriorityRadioProps {
  checkedPriority: EPriority;
  setCheckedPriority: (priority: EPriority) => void;
}
