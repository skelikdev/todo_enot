import { Radio as RadioMUI, styled } from "@mui/material";
import { IRadioProps } from "./interfaces";

export const Radio = styled(RadioMUI)<IRadioProps>`
  & svg {
    color: ${({ theme, checked, priority }) =>
      checked
        ? theme.customPalette.priority[priority].dark
        : theme.customPalette.priority[priority].light};
  }
`;
