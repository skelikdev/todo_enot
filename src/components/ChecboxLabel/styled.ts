import { FormControlLabel, styled } from "@mui/material";

export const StyledFormControlLabel = styled(FormControlLabel)`
  & .MuiTypography-root {
    ${({ theme }) => theme.typography.h5}
  }
`;
