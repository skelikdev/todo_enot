import { Checkbox } from "@mui/material";
import { FC } from "react";
import { ICheckboxLabelProps } from "./interfaces";
import { StyledFormControlLabel } from "./styled";

export const CheckboxLabel: FC<ICheckboxLabelProps> = ({
  onChange,
  checked,
  label,
}) => {
  const inputProps =
    checked === undefined ? {} : { "aria-label": "controlled" };

  return (
    <StyledFormControlLabel
      control={
        <Checkbox defaultChecked onChange={onChange} inputProps={inputProps} />
      }
      label={label}
    />
  );
};
