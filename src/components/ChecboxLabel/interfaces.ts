import { ChangeEvent } from "react";

export interface ICheckboxLabelProps {
  onChange?: (event: ChangeEvent<HTMLInputElement>) => void;
  checked?: boolean;
  label: string;
}
