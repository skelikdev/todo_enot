import { styled } from "@mui/material";
import { Card as CardUI } from "@shared/ui/Card/Card";

export const Card = styled(CardUI)`
  margin-bottom: ${({ theme }) => theme.spacing(2)};
`;
