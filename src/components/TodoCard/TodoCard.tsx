import { Switch } from "@shared/ui/Switch/Switch";
import { FC } from "react";
import { ITodoCardProps } from "./interfaces";
import { Card } from "./styled";

export const TodoCard: FC<ITodoCardProps> = ({
  isDone,
  handlerChangeIsOpen,
  id,
  ...props
}) => {
  return (
    <Card
      {...props}
      checked={isDone}
      id={id}
      actionBlock={
        <Switch
          checked={isDone}
          onChange={async () => {
            if (id) {
              await handlerChangeIsOpen(id, !isDone);
            }
          }}
        />
      }
    />
  );
};
