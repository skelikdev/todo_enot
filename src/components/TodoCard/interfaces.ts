import { ICardProps } from "@shared/ui/Card/interfaces";

export interface ITodoCardProps extends ICardProps {
  isDone: boolean;
  handlerChangeIsOpen: (id: string, done: boolean) => Promise<void>;
}
