import { CheckboxLabel } from "@components/ChecboxLabel/ChecboxLabel";
import { Card } from "@shared/ui/Card/Card";
import { ExpanderButton } from "@shared/ui/ExpanderButton/ExpanderButton";
import { PaperBox } from "@shared/ui/PaperBox/PaperBox";
import { AnimatePresence, motion } from "framer-motion";
import { FC, useState } from "react";
import { IAccordionProps } from "./interfaces";
import * as Styled from "./styled";

export const Accordion: FC<IAccordionProps> = ({ title, children }) => {
  const [isOpen, setIsOpen] = useState(false);
  const handlerChangeOpen = () => {
    setIsOpen(!isOpen);
  };
  return (
    <div className="accordionWrapper">
      <AnimatePresence>
        {isOpen && (
          <motion.div
            initial={{ opacity: 0, height: 0 }}
            animate={{ opacity: 1, height: "fit-content" }}
            exit={{ opacity: 0, height: 0 }}
          >
            <CheckboxLabel
              label={title}
              onChange={handlerChangeOpen}
              checked={isOpen}
            />
          </motion.div>
        )}
      </AnimatePresence>

      <motion.div>
        <PaperBox>
          <AnimatePresence>
            {isOpen && (
              <motion.div
                initial={{ opacity: 0, height: 0, width: "100%" }}
                animate={{ opacity: 1, height: "fit-content" }}
                exit={{ opacity: 0, height: 0 }}
              >
                {children}
              </motion.div>
            )}
          </AnimatePresence>
          <AnimatePresence>
            {!isOpen && (
              <Styled.TitleAccordion
                initial={{ opacity: 0, height: 0 }}
                animate={{ opacity: 1, height: "fit-content" }}
                exit={{ opacity: 0, height: 0 }}
              >
                <Card title={title} priorityStatus="unknown" />
                <ExpanderButton isOpen={isOpen} onClick={handlerChangeOpen} />
              </Styled.TitleAccordion>
            )}
          </AnimatePresence>
        </PaperBox>
      </motion.div>
    </div>
  );
};
