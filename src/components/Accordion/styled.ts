import { styled } from "@mui/material";
import { motion } from "framer-motion";

export const TitleAccordion = styled(motion.div)`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
`;
