import { styled } from "@mui/material";

export const MarqueeContainer = styled("div")`
  overflow: hidden;
  display: flex;
  align-items: center;
  flex-direction: row !important;
  position: relative;
  width: 100%;
  &:hover div {
    animation-play-state: paused;
  }

  & .marquee {
    flex: 0 0 auto;
    min-width: 100%;
    z-index: 1;
    display: flex;
    flex-direction: row;
    align-items: center;
    text-align: center;
    animation: scroll var(--duration) linear 1;
    animation-play-state: running;
    animation-direction: normal;
  }
  @keyframes scroll {
    0% {
      transform: translateX(0%);
    }
    100% {
      transform: translateX(-100%);
    }
  }
`;
