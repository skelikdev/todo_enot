import React from "react";

export interface MarqueeProps {
  style?: React.CSSProperties;
  speed?: number;
  children?: React.ReactNode;
  onFinish?: () => void;
}
