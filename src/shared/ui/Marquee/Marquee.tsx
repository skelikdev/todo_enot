import React, { Fragment, useEffect, useRef, useState } from "react";
import { MarqueeProps } from "src/shared/ui/Marquee/interfaces";
import * as Styled from "src/shared/ui/Marquee/styled";

export const Marquee: React.FC<MarqueeProps> = ({
  style = {},
  speed = 20,
  onFinish,
  children,
}) => {
  const [containerWidth, setContainerWidth] = useState(0);
  const [marqueeWidth, setMarqueeWidth] = useState(0);
  const [isMounted, setIsMounted] = useState(false);
  const containerRef = useRef<HTMLDivElement>(null);
  const marqueeRef = useRef<HTMLDivElement>(null);
  const [firstRender, setFirstRender] = useState(true);

  useEffect(() => {
    if (!isMounted) return;

    const calculateWidth = () => {
      if (marqueeRef.current && containerRef.current) {
        setContainerWidth(containerRef.current.getBoundingClientRect().width);
        setMarqueeWidth(marqueeRef.current.getBoundingClientRect().width);
      }
    };

    calculateWidth();
    window.addEventListener("resize", calculateWidth);
    return () => {
      window.removeEventListener("resize", calculateWidth);
    };
  }, [isMounted]);

  useEffect(() => {
    setIsMounted(true);
  }, []);

  const duration =
    marqueeWidth < containerWidth
      ? containerWidth / speed
      : marqueeWidth / speed;
  return (
    <Fragment>
      {!isMounted ? null : (
        <Styled.MarqueeContainer ref={containerRef} style={style}>
          <div
            ref={marqueeRef}
            style={{
              ["--duration" as string]: `${duration}s`,
            }}
            className="marquee"
          >
            {children}
          </div>
          <div
            style={{
              ["--duration" as string]: `${duration}s`,
            }}
            className="marquee"
            aria-hidden="true"
            onAnimationEnd={() => {
              if (firstRender) {
                setFirstRender(false);
              } else {
                onFinish && onFinish();
              }
            }}
          >
            {children}
          </div>
        </Styled.MarqueeContainer>
      )}
    </Fragment>
  );
};
