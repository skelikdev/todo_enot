import { styled } from "@mui/material";

export const ScrollBoxWrapper = styled("div")`
  width: 100%;
  flex-grow: 1;
  overflow-y: scroll;
  /* width */
  ::-webkit-scrollbar {
    width: ${({ theme }) => theme.spacing(1.5)};
  }

  /* Track */
  ::-webkit-scrollbar-track {
    background: ${({ theme }) => theme.customPalette.colors.gray.light};
    border-radius: 10px;
  }

  /* Handle */
  ::-webkit-scrollbar-thumb {
    background: ${({ theme }) => theme.customPalette.priority.high.light};
  }

  /* Handle on hover */
  ::-webkit-scrollbar-thumb:hover {
    background: ${({ theme }) => theme.customPalette.priority.high.main};
  }
`;
