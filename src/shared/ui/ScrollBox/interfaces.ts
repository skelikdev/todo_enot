import { ReactNode } from "react";

export interface IScrollBoxProps {
  children?: ReactNode;
  className?: string;
}
