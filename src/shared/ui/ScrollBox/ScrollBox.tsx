import { FC } from "react";
import { IScrollBoxProps } from "./interfaces";
import * as Styled from "./styled";

export const ScrollBox: FC<IScrollBoxProps> = ({ children, className }) => {
  return (
    <Styled.ScrollBoxWrapper className={className}>
      {children}
    </Styled.ScrollBoxWrapper>
  );
};
