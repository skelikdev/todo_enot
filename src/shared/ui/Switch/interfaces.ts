import { UseSwitchParameters } from "@mui/base/SwitchUnstyled";

export interface ISwitchProps extends UseSwitchParameters {
  className?: string;
  ariaLabel?: string;
}
