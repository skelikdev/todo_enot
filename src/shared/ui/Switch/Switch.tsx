import { useSwitch } from "@mui/base/SwitchUnstyled";
import CheckIcon from "@mui/icons-material/Check";
import CloseIcon from "@mui/icons-material/Close";
import clsx from "clsx";
import { FC } from "react";
import { ISwitchProps } from "./interfaces";
import * as Styled from "./styled";

export const Switch: FC<ISwitchProps> = ({
  ariaLabel,
  className,
  ...props
}) => {
  const { getInputProps, checked, disabled, focusVisible } = useSwitch(props);
  const stateClasses = {
    checked,
    disabled,
    focusVisible,
  };

  return (
    <Styled.SwitchRoot className={clsx(stateClasses) + ` ${className}`}>
      <Styled.SwitchTrack className={clsx(stateClasses)}>
        <Styled.SwitchThumb className={clsx(stateClasses)}>
          <CheckIcon className="SwitchCheckIcon" />
          <CloseIcon className="SwitchCloseIcon" />
        </Styled.SwitchThumb>
      </Styled.SwitchTrack>
      <Styled.SwitchInput {...getInputProps()} aria-label={ariaLabel} />
    </Styled.SwitchRoot>
  );
};
