import { styled } from "@mui/material";

export const SwitchRoot = styled("span")`
  font-size: 0;
  position: relative;
  display: inline-block;
  width: ${({ theme }) => theme.spacing(13)};
  height: ${({ theme }) => theme.spacing(8)};
  cursor: pointer;
`;

export const SwitchInput = styled("input")`
  cursor: inherit;
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  opacity: 0;
  z-index: 1;
  margin: 0;
`;

export const SwitchThumb = styled("span")`
  display: block;
  width: ${({ theme }) => theme.spacing(6)};
  height: ${({ theme }) => theme.spacing(6)};
  top: ${({ theme }) => theme.spacing(1)};
  left: ${({ theme }) => theme.spacing(1)};
  border-radius: 16px;
  background-color: ${({ theme }) => theme.customPalette.colors.white.main};
  position: relative;
  transition-property: all;
  transition-timing-function: cubic-bezier(0.4, 0, 0.2, 1);
  transition-duration: 120ms;

  &.focusVisible {
    background-color: pink;
    box-shadow: 0 0 1px 8px rgba(0, 0, 0, 0.25);
  }

  &.checked {
    left: ${({ theme }) => theme.spacing(6)};
    top: 4px;
  }

  &.checked {
    & .SwitchCloseIcon {
      opacity: 0;
    }

    & .SwitchCheckIcon {
      opacity: 1;
    }
  }

  & .SwitchCloseIcon {
    opacity: 1;
    position: absolute;
    transition: all 0.1s;
  }

  & .SwitchCheckIcon {
    opacity: 0;
    position: absolute;
    transition: all 0.1s;
  }
`;

export const SwitchTrack = styled("span")`
  border-radius: 16px;
  display: block;
  height: 100%;
  width: 100%;
  position: absolute;
  background: ${({ theme }) => theme.customPalette.colors.blue.main};
  svg {
    color: ${({ theme }) => theme.customPalette.colors.blue.main};
  }
  &.checked {
    background: ${({ theme }) => theme.customPalette.colors.green.main};
    svg {
      color: ${({ theme }) => theme.customPalette.colors.green.main};
    }
  }
`;
