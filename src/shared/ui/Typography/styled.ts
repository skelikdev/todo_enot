import { styled, Typography as TypographyMUI } from "@mui/material";
import { ITypographyStyledProps } from "./interfaces";

export const Typography = styled(TypographyMUI)<ITypographyStyledProps>`
  ${({ lineThrough }) => lineThrough && "text-decoration: line-through;"}
`;
