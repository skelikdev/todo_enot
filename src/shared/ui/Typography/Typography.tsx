import { styled, Typography as TypographyMUI, useTheme } from "@mui/material";
import { CSSProperties, FC } from "react";
import { ITypographyProps, TTypographyVariant } from "./interfaces";
import * as Styled from "./styled";
export const Typography: FC<ITypographyProps> = ({
  variant,
  children,
  lineThrough,
}) => {
  const { customTypography } = useTheme();
  const isCustomVariant = Object.keys(customTypography).includes(variant);
  if (isCustomVariant) {
    // TODO костыль, допилить тему типографики и заменить стили DefaultTheme.typography
    return (
      <Styled.Typography
        lineThrough={lineThrough}
        style={customTypography[variant as TTypographyVariant] as CSSProperties}
        variant={variant}
      >
        {children}
      </Styled.Typography>
    );
  }
  return <TypographyMUI variant={variant}>{children}</TypographyMUI>;
};
