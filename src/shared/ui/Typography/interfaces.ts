import { Variant } from "@mui/material/styles/createTypography";
import { ReactNode } from "react";

export type TTypographyVariant = "h4" | "h5" | "body1";

export interface ITypographyProps {
  variant: Variant;
  children: ReactNode;
  lineThrough?: boolean;
}

export interface ITypographyStyledProps {
  lineThrough?: boolean;
}
