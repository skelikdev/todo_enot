import { Paper as PaperMUI, styled } from "@mui/material";

export const Paper = styled(PaperMUI)`
  display: flex;
  flex-direction: column;
  padding: ${({ theme }) => theme.spacing(4, 5)};
  align-items: center;
  justify-content: center;
  background: ${({ theme }) => theme.palette.background.paper};
  width: 100%;
  border-radius: 40px;
`;
