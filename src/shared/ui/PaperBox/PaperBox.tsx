import { FC } from "react";
import { IPaperBoxProps } from "./interfaces";
import * as Styled from "./styled";

export const PaperBox: FC<IPaperBoxProps> = ({ children }) => {
  return <Styled.Paper>{children}</Styled.Paper>;
};
