import { ReactNode } from "react";

export interface IPaperBoxProps {
  children?: ReactNode;
}
