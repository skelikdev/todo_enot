import { MouseEventHandler, ReactNode } from "react";

export interface IExpanderButtonProps {
  isOpen: boolean;
  onClick?: MouseEventHandler<HTMLButtonElement>;
  children?: ReactNode;
  ariaLabel?: string;
  color?: "primary" | "secondary" | "error" | "info" | "success" | "warning";
}

export interface IIconButtonProps {
  isOpen: boolean;
}
