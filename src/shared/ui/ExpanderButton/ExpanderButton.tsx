import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { FC } from "react";
import { IExpanderButtonProps } from "./interfaces";
import * as Styled from "./styled";

export const ExpanderButton: FC<IExpanderButtonProps> = ({
  onClick,
  isOpen,
  children,
  ariaLabel,
  color,
}) => {
  return (
    <Styled.IconButton
      color={color}
      isOpen={isOpen}
      onClick={onClick}
      aria-label={ariaLabel}
    >
      {children ? children : <ExpandMoreIcon />}
    </Styled.IconButton>
  );
};
