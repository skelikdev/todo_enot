import { IconButton as IconButtonMui, styled } from "@mui/material";
import { IIconButtonProps } from "@shared/ui/ExpanderButton/interfaces";

export const IconButton = styled(IconButtonMui)<IIconButtonProps>`
  transform: rotate(${({ isOpen }) => (isOpen ? 180 : 0)}deg);
  transition: all 0.3s;
`;
