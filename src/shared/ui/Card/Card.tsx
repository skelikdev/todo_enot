import { Typography } from "@shared/ui/Typography/Typography";
import { FC } from "react";
import { ICardProps } from "./interfaces";
import * as Styled from "./styled";

export const Card: FC<ICardProps> = ({
  priorityStatus,
  id,
  checked,
  title,
  description,
  actionBlock,
  className,
}) => {
  return (
    <Styled.Wrapper id={id} className={className}>
      <Styled.PriorityLine priorityStatus={priorityStatus} />
      <Styled.TextBlock>
        <Typography variant="h5" lineThrough={checked}>
          {title}
        </Typography>
        {description && (
          <Typography lineThrough={checked} variant="body1">
            {description}
          </Typography>
        )}
      </Styled.TextBlock>
      {actionBlock}
    </Styled.Wrapper>
  );
};
