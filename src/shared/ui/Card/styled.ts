import { styled } from "@mui/material";
import { IPriorityLineProps } from "./interfaces";

export const PriorityLine = styled("div")<IPriorityLineProps>`
  background: ${({ theme, priorityStatus }) =>
    theme.customPalette.priority[priorityStatus].main};
  width: ${({ theme }) => theme.spacing(1)};
  height: ${({ theme }) => theme.spacing(10)};
  border-radius: 10px;
`;

export const TextBlock = styled("div")`
  margin-left: ${({ theme }) => theme.spacing(3.5)};
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  line-height: normal;
  flex-grow: 1;
`;

export const Wrapper = styled("div")`
  display: flex;
  width: 100%;
  align-items: center;
  height: ${({ theme }) => theme.spacing(11.5)};
`;
