import { TPriority } from "@globalTypes";
import { ReactNode } from "react";

export interface ICardProps {
  priorityStatus: TPriority;
  checked?: boolean;
  id?: string;
  title: string;
  description?: string;
  actionBlock?: ReactNode;
  className?: string;
}

export interface IPriorityLineProps {
  priorityStatus: TPriority;
}
